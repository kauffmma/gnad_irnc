# GNAD-IRNC repository

## Presentation
This repository aims to expand on and contribute to the GNAD (Global Nonviolent Action Database) project initiated at Swarthmore College under this initial objective:

"Why this database?

- To provide free access to information about hundreds of cases of nonviolent action for learning and for citizen action. We research campaigns that have reached a point of completion. The cases are drawn from all continents and most countries.  People are shown struggling for human rights, economic justice, democracy, national and ethnic identity, environmental sustainability, and peace.  To learn more about searching by the issues people are struggling about, go to Issue Clusters.

- To make available comparative information that will support researchers and writers to develop strategic knowledge and theory.  Each case is presented in two formats: the database file (with searchable fields) and the narrative describing the struggle as an unfolding story. The database supports searches by country, by issue, by action method used, and even by year -- there is a case from ancient Egypt, 12th century BCE!  Some cases are part of a “wave” of campaigns, such as the “Arab Awakening” of 2011; by pressing each “wave” button one can find cases.
    
George Lakey 12/08/2011 (https://nvdatabase.swarthmore.edu/content/about-database)"

This repository aims at providing software tools to:

- Help translate the GNAD into languages other than English
- Help expand the GNAD with new data (for existing campaigns)
- Help expand the GNAD with new campaigns
- Build a website with advance, interactive functions, including charts, graphics, text information, tables...

## Choice of software tools

It is aimed to use only free, libre and open-source software (FLOSS) in this project.

## License

The software in this repository is released under the GNU AFFERO GENERAL PUBLIC LICENSE v3 or later.
Data from the Global Nonviolent Action Database (or GNAD, managed by Swarthmore College) are licensed under a Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International license unless otherwise noted (see https://nvdatabase.swarthmore.edu, https://nvdatabase.swarthmore.edu/content/how-cite-cases and https://creativecommons.org/licenses/by-nc-nd/4.0/).
Other data on non-violent campaigns (that is, data not coming from the GNAD) are licensed under an Attribution-NonCommercial-ShareAlike 4.0 International license (CC BY-NC-SA 4.0, https://creativecommons.org/licenses/by-nc-sa/4.0/)


## Authors and acknowledgment / auteurs et remerciements

Chaque année, environ 60 étudiants issus de plusieurs filières universitaires françaises (Université Grenoble Alpes, Université Savoie Mont-Blanc…) réalisent un exercice de contribution à la base de données des campagnes d’action non-violentes GNAD, comme travail d'initiation à la recherche qualitative et aux bases de données en sciences sociales.

Ce travail a été rendu possible grâce à diverses organisations : 
• IRNC (Institut de recherche sur la Résolution Non-violente des Conflits ; association loi 1901 créée en 1984). https://www.irnc.org/ 
• Ecole de la paix de Grenoble (association loi 1901 créée en 1998). https://www.ecoledelapaix.org/ 
• Universités et institutions d'enseignement supérieur: 
- En France et en Colombie (pilotage IRNC/Ecole de la paix): Université Savoie Mont Blanc, Université Grenoble Alpes, Institut Catholique de Paris, Hybria (Lyon), UTS (Unidades Tecnológicas de Santander, Bucaramanga), UIS (Universidad Industrial de Santander, Bucaramanga), EAFIT (Medellin). 
- Aux États-Unis et au Canada (pilotage Swarthmore College): Tufts University, Georgetown University, Menno Simons College Winnipeg, Canadian Mennonite University; plus de 200 contributeurs, liste à la page https://nvdatabase.swarthmore.edu/content/who-made-database 

Financements et contributions en nature: 
• Fonds Non Violence 21 (https://nonviolence21.org ) / Fondation Un monde par tous 
• Universités (indemnités d’enseignement, locaux...) 
• Grenoble Alpes Cybersecurity Institute 
• Ecole de la paix de Grenoble 
• Collectivités territoriales (Grenoble, Montmélian) 
• Grésivaudan Peace Orchestra 
• Ministère des Affaires Étrangères 
• CESICE (Centre d’Études sur la Sécurité Internationale et les Coopérations Européennes, UGA) 
• COLIFRI (Cumbre Colombo – Francesa de Investigación - Assises franco-colombiennes de la recherche) 
• Bonitasoft 
• Kaizen Solutions 
• Ministère des armées (projet CYBIS UGA « Cybersécurité et sécurité internationale – données, modélisation et visualisation »), Pré-labellisation Labex « Label Centre d'Excellence » 2018-2020. 

Les avis exprimés sur ce site et dans la base de données n'engagent que leurs auteurs.

Coordination du projet: Mayeul Kauffmann.

## Contributing
Contributions are welcome. Please follow the following guidelines.

### Naming convention

#### Table names

Use lower-case letters to name database objects. For separating words in the database object name, use underscore, for instance: file_history.

Use the singular for a table name, not the plural; for instance: user, role...

For tables which serve as the link in a n-to-m relationship, the table which comes first in alphabetical order is listed first, followed by two underscores; for instance role_user (not user_role).

Localized tables have the full name of the original table, suffixed by _loc: wave_loc (column prefix: wavl_, see below).

Denormalized tables have the full name of the original table, suffixed by _den: event_den (column prefix: eved_, see below). Denormalized tables are temporary tables that are used for loading data from flat files in a single operation.



#### Prefix for column names

All fields are prefixed by four letters (proper to the table) and a underscore.

The four letters are usually the first letters of the table name (e.g. 'camp' for table 'campaign'). For table names with  names in two parts (such as file_history), the first two letters are taken from the first word, the next two come from the second word (fihi_ in the case of file_history; rous_ for role_user), with the exceptions that localized tables takes only the 'l' from 'loc' (hence, the prefix for wave_loc is wavl), and denormalized tables takes only the 'd' from 'den' (columns of table event_den start with 'eved_'). For table name with three words, two letters come from the first word, the rest from the other words.

This prefix convention applies as long as the names remain unique, otherwise other combinations are searched for, using subsequent letters in the words (consonnants are preferred in this case).

A primary key column is named with the table prefix followed by _id. 

Names of foreign key columns are: {prefix table 1}_{column_name_in_table_2}, for instance: team_caml_id is the field in the 'team' table that references the caml_id (FK) from the campaign_loc table.


#### Constraints

Postgresql constraints are named {table_name}_YY where YY are two letters (lower-case) such as pk, ak, ix, ck, df, fk (names taken from https://stackoverflow.com/a/4838940).

If needed, sequential numbers {xx} can be added (one_table_ck01).

| Type      | Naming pattern      | Comment      |
| ---      | ---      | ---      |
|Primary key |{table_name}_pk||
|Unique index/constraint |{table-name}_ak{xx} |  AK: Alternate Key|
|Non-Unique index |{table_name}_ix{xx}||
|Check constraint |{table_name}_ck{xx}||
|Default constraint |{table_name}_df{xx}||
|Foreign key constraint |{table_name}_fk||


Hence, a primary key constraint is of the form {tablename}_pk  (e.g. wave_pk).

Foreign key constraints take a name formed as: {tablename 1}_{tablename 2}_fk (tables names are not abbreviated here).  Table 1 is the child, on the n-side of the 1-n relationship, as this is where the constraint lies. In the campaign table, the constraint on camp_wave_id is camp_wave_fk. For the special case of a 1-1 relationship (such as user and user_private tables, for privacy reasons), {tablename 1} is the table where the constraint lies (here, the constraint is named: user_private_user_fk).

There are no numbers for foreign key constraint (we use the table name of the second table), contrary to what is suggested at https://stackoverflow.com/a/4838940.

For a unique constraint on one or two columns or more (max total length name: 63 bytes):
{tablename}_{columnname1}_{columnname2}_ak.

For instance, for a unique constraint on a single column, we may have wave_url_ak.

We use a number for unique constraints with 3 or more column names, or if the above is more than 63 bytes.
In this case: unique constraint: {tablename}_ak{nn}.



## Installation

(In progress. Instructions to get the website working.)


## Roadmap
(In progress.)


